package com.ozgurbircan.blog.controller;

import com.ozgurbircan.blog.model.Entry;
import com.ozgurbircan.blog.repository.EntryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Optional;
@Slf4j
@Controller
@RequestMapping("/entry")
public class TestController
{
    @Autowired
    EntryRepository entryRepository;


    @RequestMapping(value = "",method = RequestMethod.GET)
    public String getAllIndex(Model model)
    {
        Iterable<Entry> entryIterable=entryRepository.findAll();
        model.addAttribute("entries",entryIterable);
        return "entries/listEntries.html";
    }


    @RequestMapping(value = "/new",method = RequestMethod.GET)
    public String getEntryForm(Model model)
    {
        model.addAttribute("entry",new Entry());

        return "entries/newEntry";
    }

    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    public String getId(@PathVariable("id") Integer id,Model model)
    {
        Optional<Entry> entryOptional= entryRepository.findById(id);
        if(!entryOptional.isPresent())
        {
            log.warn("Entry with {} id is no present",id);
            return "index";
        }

        else
        {
            model.addAttribute("entry",entryOptional.get());
            return "entries/showEntry";

        }
    }

}
