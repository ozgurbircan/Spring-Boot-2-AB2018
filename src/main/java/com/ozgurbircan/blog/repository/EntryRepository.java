package com.ozgurbircan.blog.repository;

import com.ozgurbircan.blog.model.Entry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EntryRepository extends CrudRepository<Entry,Integer>
{


}
